package com.example.myapplication;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by kesavajawaharlal on 07/10/2013.
 */
public class MapFragment extends SupportMapFragment {

    private static final String TAG = "Route map";
    private GoogleMap map;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        map = this.getMap();

    }


}